def askNumber(text):
    try:
        return int(input(text))
    except:
        print("La valeur n'est pas un nombre")
        return askNumber(text)

if __name__ == '__main__':
    somme = askNumber('Saisissez un premier nombre : ') + askNumber('Saisissez un deuxième nombre : ')
    print('La somme des deux valeurs font : ' + str(somme))