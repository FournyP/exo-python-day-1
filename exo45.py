def askSentence():
    return input('Saisir une phrase : ')

def getNumberOfUpperAndLower(s):
    lower = 0
    upper = 0

    for char in s:
        if char.isupper():
            upper = upper + 1
        if char.islower():
            lower = lower + 1

    return lower, upper

if __name__ == "__main__":
    
    s = askSentence()

    res = getNumberOfUpperAndLower(s)

    print(res)