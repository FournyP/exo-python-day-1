def askNumber():
    try:
        return int(input('Saisir un nombre : '))
    except:
        print("La valeur n'est pas un nombre")
        return askNumber()

if __name__ == "__main__":
    
    number = askNumber()

    res = []

    for index in range(1, number + 1):
        if number % index == 0:
            res.append(index)

    print(res)