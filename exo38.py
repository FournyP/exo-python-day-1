def askSentence():
    return input('Saisir une phrase : ')

if __name__ == "__main__":
    
    sentence = askSentence()

    words = sentence.split()

    tmp = words[-1]
    words[-1] = words[0]
    words[0] = tmp

    res = " ".join(words)

    print(res)