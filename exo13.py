def askNumber():
    try:
        return int(input('Saisir un nombre : '))
    except:
        print("La valeur n'est pas un nombre")
        return askNumber()

if __name__ == "__main__":
    
    a = askNumber()
    b = askNumber()

    print("Le quotien est : " + str(int(a/b)))
    print("Le reste est : " + str(a%b))