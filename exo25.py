def askWord():
    return input('Saisir un mot : ')

if __name__ == "__main__":
    
    word = askWord()

    res = ""

    for index in range(len(word) - 1, -1, -1):
        res = res + word[index]

    print(res)