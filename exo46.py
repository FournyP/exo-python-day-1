def convertNumberToDigit(number):
    
    if number == 0:
        return [0]

    l = []
    while number > 0:
        l.append(number % 10)
        number = number // 10
    return l

if __name__ == "__main__":
    
    print(convertNumberToDigit(256))