def askWord():
    return input('Saisir un mot : ')

def InsertEtoile(word):
    res = ""

    for index in range(0, len(word)):
        if index != len(word) - 1:
            res = res + word[index] + "*"
        else:
            res = res + word[index]

    return res


if __name__ == "__main__":
    
    word = askWord()
    res = InsertEtoile(word)
    print(res)