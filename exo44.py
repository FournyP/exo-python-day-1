def toutEnMajuscule(L):
    res = []
    for item in L:
        res.append(item.upper())

    return res

if __name__ == "__main__":
    
    L = ["Python", "est", "un", "langage", "de", "programmation"]

    L2 = toutEnMajuscule(L)

    print(L2)