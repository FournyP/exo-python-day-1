def compareList(list1, list2):
    res = []
    for item1 in list1:
        for item2 in list2:
            if item1 == item2:
                res.append(item1)

    return res

if __name__ == "__main__":
    
    list1 = [1, 2, 3, 4, 5, 6]
    list2 = [6, 7, 8, 9, 10, 11]

    print(compareList(list1, list2))