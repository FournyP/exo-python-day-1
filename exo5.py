def askYear():
    try:
        year = int(input('Saisissez votre âge : '))
        if year < 0:
            print('Impossible')
            return askYear()
        return year
    except:
        print("La valeur saisie n'est pas un nombre")
        return askYear()

if __name__ == "__main__":
    year = askYear()

    if year >= 18:
        print("Vous êtes majeur !")
    else:
        print("Vous êtes mineur !")