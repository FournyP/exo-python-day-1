def askWord():
    return input('Saisir un mot : ')

if __name__ == "__main__":
    
    word = askWord()

    treatedChar = []
    res = []

    for char in word:
        if char not in treatedChar:
            occurance = word.count(char)
            res.append("Le caractère : '" + char + "' figure " + str(occurance) + " fois dans la chaine")
            treatedChar.append(char)

    print(res)

