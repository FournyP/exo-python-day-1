def askWord():
    return input('Saisir une chaine : ')

if __name__ == "__main__":

    word = askWord()

    res = ""
    
    for index in range(0, len(word)):
        if index % 2 == 0:
            res = res + word[index]

    print(res)