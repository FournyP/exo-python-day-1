from math import *

def askRadius():
    try:
        return int(input('Saisir un rayon : '))
    except:
        print("La valeur n'est pas un nombre")
        return askRadius()
    
if __name__ == "__main__":
    
    radius = askRadius()

    surface = radius**2 * pi
    perimeter = 2 * pi * radius

    print("La surface est égale à ", surface)
    print("Le périmetre est égale à ", perimeter)