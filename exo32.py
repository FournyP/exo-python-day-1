def permuteList(liste):
    res = []

    for index in range(len(liste) -1, -1, -1):
        res.append(liste[index])

    return res

if __name__ == "__main__":
    
    list1 = [1, 2, 3, 4, 5, 6]

    permutedList = permuteList(list1)

    print(permutedList == [6, 5, 4, 3, 2, 1])