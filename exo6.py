def askNumber(text):
    try:
        return int(input(text))
    except:
        print("La valeur n'est pas un nombre")
        return askNumber(text)
        
if __name__ == "__main__":
    x = askNumber("Saisir un premier nombre : ")
    y = askNumber("Saisir un deuxième nombre : ")
    z = askNumber("Saisir un troisième nombre : ")

    print(str(max([x, y, z])) + " est le plus grand nombre")