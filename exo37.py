def askSentence():
    return input('Saisir une phrase : ')

if __name__ == "__main__":
    
    wordsFirstSentence = askSentence().split()
    wordsSecondSentence = askSentence().split()

    res = []
    for item1 in wordsFirstSentence:
        for item2 in wordsSecondSentence:
            if item1 == item2:
                res.append(item1)

    print(res)