def askWord():
    return input('Saisir un mot : ')

if __name__ == "__main__":
    
    voyelles = ['a', 'e', 'i', 'o', 'u', 'y']

    word = askWord()
    count = 0

    for char in word:
        if char in voyelles:
            count = count + 1

    print("La chaine contient : " + str(count) + " voyelles")