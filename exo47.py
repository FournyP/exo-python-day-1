def askSentence():
    return input('Saisir une phrase : ')

def getCommunWords(sentence1, sentence2):
    
    wordsFirstSentence = sentence1.split()
    wordsSecondSentence = sentence2.split()

    res = []
    for item1 in wordsFirstSentence:
        for item2 in wordsSecondSentence:
            if item1 == item2:
                res.append(item1)

    return res

if __name__ == "__main__":

    sentence1 = askSentence()
    sentence2 = askSentence()
    
    print(getCommunWords(sentence1, sentence2))