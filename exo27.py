def askSentence():
    return input("Saisir une phrase : ")

if __name__ == "__main__":

    sentence = askSentence()

    words = sentence.split()

    res = ""

    for word in words:
        if(len(word) > len(res)):
            res = word
        
    print(res)
