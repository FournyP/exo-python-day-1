def deleteDuplicate(nbrList):
    return list(set(nbrList))

if __name__ == "__main__":
    
    nbrList1 = [1, 2, 3, 3, 4, 5, 6]
    nbrList2 = ['a', 'b', 'c', 'c', 'd', 'e']
    print(deleteDuplicate(nbrList1))
    print(deleteDuplicate(nbrList2))