def askNumber(text):
    try:
        return int(input(text))
    except:
        print("La valeur n'est pas un nombre")
        return askNumber(text)

if __name__ == "__main__":
    number = askNumber('Saisissez un premier nombre : ')

    if number % 2 == 0:
        print("Le nombre est pair")
    else:
        print("Le nombre est impair")