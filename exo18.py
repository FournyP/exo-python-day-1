def askWord():
    return input('Saisir un mot : ')

if __name__ == "__main__":
    
    word = askWord()

    res = []

    for index in range(0, len(word)):
        if word[index] == 'a':
            res.append("La lettre 'a' se trouve à la position : " + str(index))

    print(res)