def askNumber():
    try:
        number = int(input('Saisir un nombre : '))
        if number < 1:
            print("Impossible")
            return askNumber()
        return number
    except:
        print("La valeur n'est pas un nombre")
        return askNumber()

if __name__ == "__main__":
    
    number = askNumber()

    res = []

    for index in range(1, number + 1):
        if number % index == 0:
            res.append(index)

    if len(res) == 2 or number == 1:
        print("Le nombre est premier")
    else:
        print("Le nombre n'est pas premier")