def separateNumber(liste):
    res = []
    res.append([])
    res.append([])
    for item in liste:
        if item%2 == 0:
            res[0].append(item)
        else:
            res[1].append(item)
        
    return res

if __name__ == "__main__":
    
    list1 = [1, 2, 3, 4, 5, 6]

    res = separateNumber(list1)

    print(res[0][0] == 2)
    print(res[0][1] == 4)
    print(res[0][2] == 6)

    print(res[1][0] == 1)
    print(res[1][1] == 3)
    print(res[1][2] == 5)