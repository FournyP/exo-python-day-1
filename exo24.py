def askPalindrome():
    return input('Saisir un mot : ')

if __name__ == "__main__":
    
    word = askPalindrome()

    index = 0
    isAPalindrome = True

    while(index < (len(word)/2 + 1) and isAPalindrome):
        if(word[index] != word[len(word) - 1 - index]):
            isAPalindrome = False
        index = index + 1

    if(isAPalindrome):
        print("Le mot est un palindrome")
    else:
        print("Le mot n'est pas un palindrome")
