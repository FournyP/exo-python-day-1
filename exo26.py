def askSentence():
    return input('Saisie une phrase : ')

if __name__ == "__main__":
    
    sentence = askSentence()

    words = sentence.split()

    res = []

    for word in words:
        if word[0] == 'a':
            res.append(word)

    print(res)