def nombreOccurences(L, x):
    res = 0
    for item in L:
        if item == x:
            res = res + 1

    return res

if __name__ == "__main__":
    
    listTest = [1, 2, 3, 3, 4, 5, 6]

    print(nombreOccurences(listTest, 3))
    print(nombreOccurences(listTest, 7))