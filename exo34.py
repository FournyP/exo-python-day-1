def getNotesGreatherOrEqualThanTen(notes):
    res = []

    for note in notes:
        if note >= 10:
            res.append(note)

    return res

if __name__ == "__main__":
    notes = [12 , 4 , 14 , 11 , 18 , 13 , 7, 10 , 5 , 9 , 15 , 8 , 14 , 16]

    res = getNotesGreatherOrEqualThanTen(notes)

    print(res)