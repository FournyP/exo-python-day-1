def nombreDivisibles(liste, n):
    res = []
    for item in liste:
        if item % n == 0:
            res.append(item)

    return res

if __name__ == "__main__":
    
    listTest = [1, 2, 3, 4, 5, 6]

    print(nombreDivisibles(listTest, 2))
    print(nombreDivisibles(listTest, 3))