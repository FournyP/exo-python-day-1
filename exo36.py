import re

def askSentence():
    return input('Saisir une phrase : ')

if __name__ == "__main__":
    
    sentence = askSentence()

    res = re.sub(' +', ' ', sentence)

    print(res)