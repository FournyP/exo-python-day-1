def listIsEmpty(list):
    if len(list) == 0:
        return True
    else:
        return False

def strIsEmpty(str):
    if len(str) == 0:
        return True
    else:
        return False

if __name__ == "__main__":
    
    print(strIsEmpty(""))
    print(listIsEmpty([]))