def askFileName():
    res = input('Saisir un nom de fichier : ')

    if res.count('.') == 0:
        print("La saisie ne comprend pas d'extension")
        return askFileName()

    return res

if __name__ == "__main__":
    
    fileName = askFileName()
    parts = fileName.split('.')
    print(parts[-1])