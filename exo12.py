def defineTable(tableNumber):
    res = []

    for index in range(1, 11):
        value = str(index) + " x " + str(tableNumber) + " = " + str(index * tableNumber)
        res.append(value)
    
    return res

if __name__ == "__main__":

    for index in range(1, 10):
        print(defineTable(index))