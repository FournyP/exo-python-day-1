def askNumber(text):
    try:
        return int(input(text))
    except:
        print("La valeur n'est pas un nombre")
        return askNumber(text)

if __name__ == "__main__":
    firstNumber = askNumber('Saisissez un premier nombre : ')
    secondNumber = askNumber('Saisissez un deuxième nombre : ')

    if firstNumber > secondNumber:
        print(str(firstNumber) + " est le plus grand des deux nombres")
    elif firstNumber < secondNumber:
        print(str(secondNumber) + " est le plus grand des deux nombres")
    else:
        print("Les deux nombres sont égaux")