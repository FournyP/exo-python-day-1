from math import *

def askNumber():
    try:
        return int(input('Saisir un nombre : '))
    except:
        print("La valeur n'est pas un nombre")
        return askNumber()

if __name__ == "__main__":
    
    n = askNumber()

    if int(sqrt(n)) == sqrt(n):
        print("Le nombre est carré parfait")
    else:
        print("Le nombre n'est pas carré parfait")