def askSentence():
    return input('Saisir une phrase : ')

if __name__ == "__main__":
    
    sentence = askSentence()

    occurances = {}

    for char in sentence:
        if char not in occurances:
            occurances[char] = 0

        occurances[char] = occurances[char] + 1

    print(occurances)